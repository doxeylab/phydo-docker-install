
#Pre-requisite:


**Docker** is needed as a virtualbox to encapsulate PhyDo. Installation requires **root** access of your host machine.

**Ubuntu(>=14.04) Install**

Download docker by running `curl -fsSL get.docker.com -o get-docker.sh; sudo sh get-docker.sh`


**Mac Install**

Download docker at (https://docs.docker.com/docker-for-mac/)[https://docs.docker.com/docker-for-mac/] and follow instructions.

Now, make sure Docker is *closed*. Run the following code on terminal

```
# install homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install qemu
cd ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/
qemu-img info Docker.qcow2   # shows you current disk size
qemu-img resize Docker.qcow2 +70G  # make it 70GB larger
qemu-img info Docker.qcow2    # check the new size
```

Start docker again and proceed to installation

**Windows**
Unfortunately windows is not supported

#Installation:

download this project and change directory to the project root
```
wget https://bitbucket.org/doxeylab/phydo-docker-install/get/93c8f7981f11.zip
unzip 93c8f7981f11.zip
cd doxeylab-phydo-docker-install-93c8f7981f11

```

run the following script: (It will download ~9Gb of file, it's going to take a while)

```
sudo docker build  -t phydo:current . | tee build.log && sudo docker run -p 31415:8000 -p 31416:5000 --name phydo_container -d --rm phydo:current
```

You now have access to PhyDo running in `localhost:31415` in your web browser



#Common Docker Commands

**error during installation**

Rerun the installation command
```
sudo docker build -t phydo:current . | tee build.log && sudo docker run -p 31415:8000 -p 31416:5000 --name phydo_container -d --rm phydo:current
```

**Delete containers**
```
sudo docker rm -f phydo_container
sudo docker rmi -f phydo:current
```

In case your machine is **rebooted**, or **error** occurred, restart docker container by running 
```
sudo docker rm -f phydo_container # removes the existing container, if any
# restart the container
sudo docker run -p 31415:8000 -p 31416:5000 --name phydo_container -d --rm phydo:current
```

**Access container shell**

If you want to look at the database schema, code etc, you can start a shell inside container by
```
sudo docker exec -ti phydo_container bash
# you are now attached in container
# user Ctrl+P Ctrl+Q to detach
```

PhyDo HTML is at `/var/www/phydo-frontend`

Phydo Backend is at `/var/www/phydo-backend`

Access database by `mysql`, there is no default password
